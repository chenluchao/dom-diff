import createElem from './createElement'
import updataChildren from './updataChildren'
export default function patchVnode(oldVnode,newVnode) {
  console.log('是同一个节点')
  // 判断新旧vnode是不是同一个对象
  if (oldVnode === newVnode) return
  // 判断newVnode有没有text属性
  if (
    newVnode.text != undefined &&
    (newVnode.children == undefined || newVnode.children.length == 0)
  ) {
    console.log('新vnode有text属性')
    if (newVnode.text != oldVnode.text) {
      // 如果新虚拟节点中text和老的虚拟节点中text不同直接让新的text写入老的elm即可，即使老的有children也会消失
      oldVnode.elm.innerText = newVnode.text
    }
  } else {
    // 新的没有text,有children
    console.log('新vnode没有text属性')
    // 判断老的有没有children
    if (oldVnode.children != undefined && oldVnode.children.length > 0) {
      // 老的有children新的也有
      updataChildren(oldVnode.elm,oldVnode.children,newVnode.children)
    } else {
      // 老的没有children
      // 清空老的节点
      oldVnode.elm.innerHTML = ''
      // 遍历新的vnode的值节点，创建DOM，上树
      for (let i = 0; i < newVnode.children.length; i++) {
        let dom = createElem(newVnode.children[i])
        oldVnode.elm.appendChild(dom)
      }
    }
  }
}
