import vnode from './vnode';

// 编写一个低配版本的h函数，这个函数必须接收3个参数，缺一不可
// 相当于它的重载功能较弱
// 形态1：h('div',{},'文字')
// 形态2：h('div',{},[])
// 形态3：h('div',{},h())
export default function(sel,data,c){
  // 检查参数个数
  if(arguments.length != 3)
    throw new Error('对不起，我们是低配版h函数，必须传3个参数')
  // 检查参数c的类型
  if(typeof c == 'string' || typeof c == 'number'){
    // 说明现在调用h函数形态1
    return vnode(sel,data,undefined,c,undefined)
  }else if(Array.isArray(c)){
    // 形态2
    let children = []
    for(let i=0;i<c.length;i++){
      // 检查c[i]是不是对象
      if(!(typeof c[i] == 'object' && c[i].hasOwnProperty('sel')))
        throw new Error('传入的数组中有不少h函数的项！')
      // 这里不用执行c[i]，因为你的调用中已经执行了
      // 只需要收集就可以
      children.push(c[i])
    }
    return vnode(sel,data,children,undefined,undefined)
  }else if(typeof c == 'object' && c.hasOwnProperty('sel')){
    // 形态3
    // 传入的c是唯一的children不用执行c因为测试语句已经执行完了
    let children = [c]
    return vnode(sel,data,children,undefined,undefined)
  }else{
    throw new Error('传入的第三个参数类型错误！')
  }
}