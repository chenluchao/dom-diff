import h from './h'
import patch from './patch'

const myVnode1 = h('ul',{},[
  h('li',{},'苹果'),
  h('li',{},'香蕉'),
  h('li',{},[
    h('ol',{},[
      h('li',{},'哈哈'),
      h('li',{},'呵呵'),
    ])
  ]),
])

const container = document.getElementById('container')
patch(container,myVnode1)

const myVnode2 = h('section',{},[
  h('li',{},'哈哈'),
  h('li',{},'呵呵'),
])
const btn = document.getElementById('btn')
btn.onclick = function(){
  patch(myVnode1,myVnode2)
}