import {
  init,
  classModule,
  propsModule,
  styleModule,
  eventListenersModule,
  h,
} from "snabbdom";

const patch = init([
  // Init patch function with chosen modules
  classModule, // makes it easy to toggle classes
  propsModule, // for setting properties on DOM elements
  styleModule, // handles styling on elements with support for animations
  eventListenersModule, // attaches event listeners
]);

var vnode1 = h('div',{},[
  h('p',{key:'A'},'A'),
  h('p',{key:'B'},'B'),
  h('p',{key:'C'},'C'),
  h('p',{key:'D'},'D'),
])

const container = document.getElementById('container')
const btn = document.getElementById('btn')
patch(container,vnode1)

var vnode2 = h('div',{},h('section',{},[
  h('p',{key:'E'},'E'),
  h('p',{key:'A'},'A'),
  h('p',{key:'B'},'B'),
  h('p',{key:'C'},'C'),
  h('p',{key:'D'},'D'),
]))
// 点击按钮将vnode1变为vnode2
btn.onclick = function(){
  patch(vnode1,vnode2)
}
/* 
  diff算法
    最小量更新，key很重要，key是这个节点的唯一标识，告诉diff算法，在更改前后他们是同一个DOM节点。在规定了key的情况下哪怕打乱顺序也是在原有基础上做更改
    只有是同一个虚拟节点，才进行比较，否则就是暴力删除旧的，插入新的。
    只进行同层比较，不会进行跨层比较
*/