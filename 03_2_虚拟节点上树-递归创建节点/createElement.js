// 真正创建节点,将vnode创建为DOM，插入到pivot之前
export default function createElem(vnode){
  console.log('目标是把虚拟节点',vnode,'真正变为DOM')
  let domNode = document.createElement(vnode.sel)
  // 有子节点还是文本
  if(vnode.text!='' && (vnode.children==undefined||vnode.children.length==0)){
    // 内部是文字
    domNode.innerText = vnode.text
  }else if(Array.isArray(vnode.children) && vnode.children.length>0){
    // 它内部是子节点，就要递归创建节点
    for(let i=0;i<vnode.children.length;i++){
      let ch = vnode.children[i]
      console.log(ch)
      // 创建初它的DOM，一旦调用createElem意味着：创建出DOM了，并且它的elm属性指向了创建出的DOM但是还没有上树，是一个孤儿节点
      let chDOM = createElem(ch)
      domNode.appendChild(chDOM)
    }
  }
  // 补充elm属性
  vnode.elm=domNode
  // 返回elm,elm是纯DOM对象
  return vnode.elm
}