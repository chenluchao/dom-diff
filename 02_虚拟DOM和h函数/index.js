import {
  init,
  classModule,
  propsModule,
  styleModule,
  eventListenersModule,
  h,
} from "snabbdom";

// 创建patch函数
const patch = init([classModule,propsModule,styleModule,eventListenersModule])
// 创建虚拟节点
var myVnode1 = h('a',{props:{
  href:'http://baidu.com',
  target:'_blank'
}},'百度')
console.log(myVnode1)

const myVnode2 = h('div','我是一个盒子')

const myVnode3 = h('ul',[
  h('li','苹果'),
  h('li',[
    h('span',{class:{'li':true}},'榴莲'),
    h('span',{class:{'li':true}},'火龙果')
  ]),
  h('li',h('span','香蕉'),),
])

// 让虚拟节点上树
const container = document.getElementById('container')
patch(container,myVnode3)