// 虚拟节点上树
import vnode from './vnode'
import createElem from './createElement'
import patchVnode from './patchVnode'
export default function(oldVnode,newVnode){
  // 判断传入第一个参数，是DOM节点还是虚拟节点
  if(oldVnode.sel == '' || oldVnode.sel == undefined){
    // DOM节点，此时要包装为虚拟节点
    console.log(oldVnode)
    oldVnode = vnode(oldVnode.tagName.toLowerCase(),{},[],undefined,oldVnode)
    console.log(oldVnode)
  }
  // 判断oldVnode和newVnode是不是同一个节点
  if(oldVnode.key == newVnode.key && oldVnode.sel == newVnode.sel){
    patchVnode(oldVnode,newVnode)
  }else{
    console.log('不是同一个节点，暴力操作：先插入新的，后删除旧的！因为插入要有标杆')
    let newVnodeElm = createElem(newVnode)
    // 插入老节点之前
    if(oldVnode.elm&&newVnodeElm){
      oldVnode.elm.parentNode.insertBefore(newVnodeElm,oldVnode.elm)
    }
    // 删除老节点
    oldVnode.elm.parentNode.removeChild(oldVnode.elm)
  }
}