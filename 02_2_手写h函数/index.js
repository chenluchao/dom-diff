import h from './h'

var myVnode1 = h('div',{},'文字')
var myVnode2 = h('div',{},[
  h('p',{},'哈哈'),
  h('p',{},'呵呵')
])
var myVnode3 = h('div',{},h('p',{},'哈哈'))
console.log(myVnode1)
console.log(myVnode2)
console.log(myVnode3)